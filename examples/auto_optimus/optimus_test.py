from pathlib import Path
from filecmp import cmp
from shutil import copyfile


def test():
    """test."""

    PARENT = Path(__file__).parent / 'ressources'
    script = str(PARENT / "many_functions.py")
    s_target = str(PARENT / "many_functions_TARGET.py")
    s_clean = str(PARENT / "many_functions_CLEAN.py")
    tests = str(PARENT / "many_functions_test.py")

    copyfile(s_clean, script)
    try:
        from optimus import Optimus
        OP = Optimus(
            script,
            tests,
            verbose=False
        )
        OP.optimize()
    finally:
        test = False
        if cmp(script, s_target):
            test = True

        copyfile(s_clean, script)

        return test
