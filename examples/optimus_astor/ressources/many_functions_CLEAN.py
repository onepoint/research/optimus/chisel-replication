def f1():
    a = 0
    for _ in range(3):
        a += 1
    return a


def f2():
    print('Woot?')


def f3():
    print('42 and you know it')


def main():
    return f1()
