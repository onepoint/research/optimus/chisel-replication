from pathlib import Path
import re
import sys


def extract_module(module):
    """
    Given a module name, get a list containing the module itself and all its submodules

    :param module: the string name of the target module
    :returns: the list of modules
    """

    # Get the folder name without additionnal path
    module_name = Path(module).stem
    # Extract the package and all its submodules/packages
    # Using re to match only beginning name
    list_key = [key for key in sys.modules if re.match(module_name, key)]
    return list_key


def delete_modules(modules):
    """
    Delete every module whose name is in the input list

    :param module: list of module names to delete
    """

    for key in modules:
        del sys.modules[key]


def save_modules_state():
    """
    Create a backup of the current state of imported modules.

    :returns: a dictionnary of the module names and their corresponding modules
    """

    return {key: mod for key, mod in sys.modules.items()}


def restore_module(dic_modules):
    """
    Restore modules states using a back up dictionnary (save_modules_state)

    :param dic_modules:
    """

    for key, value in dic_modules.items():
        sys.modules[key] = value


def restore_previous_module_state(prev_modules):
    """
    Restore modules states using dictionnary. If a module is not in the list, it
    is deleted.

    :param prev_modules:
    """

    # Use of a temporary list because we can't delete an element of an iterable being iterated
    mod_to_del = []
    # Remove newly appeared modules
    for key in sys.modules:
        if key not in prev_modules:
            mod_to_del.append(key)
    for mod in mod_to_del:
        del sys.modules[mod]

    # Restore modules which could have been deleted
    for key in prev_modules:
        if key not in sys.modules:
            sys.modules[key] = prev_modules[key]
